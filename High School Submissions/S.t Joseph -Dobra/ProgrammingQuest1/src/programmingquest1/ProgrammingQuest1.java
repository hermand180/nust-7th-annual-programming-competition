package programmingquest1;
import java.util.Scanner;//THIS ALLOWS US TO GET USER INPUT

public class ProgrammingQuest1 {//THIS IS THE NAME OF OUR CLASS

   
    public static void main(String[] args) { //THIS IS THE MAIN METHOD IN WHICH OUR PROGRAMME WILL BE RUN
        
        Scanner sc = new Scanner(System.in); //ALLOWS US TO GET USER INPUT(CALLING THE FUNCTION)
        System.out.println("Enter item name: ");
        String itemName=sc.next();
        System.out.println("Enter the QTY of "+itemName+":");
       float qtyAmount=sc.nextInt();
        System.out.println("Enter price per item N$: ");
        float Price=sc.nextFloat();
        System.out.println("Amount Tendered: ");
       float amountTendered = sc.nextFloat();
       float costOfItems = Price*qtyAmount;
       float change = amountTendered-costOfItems;
        System.out.println("Your change is: "+change);
        int count20=0,count10=0,count5=0,count1=0,count50c=0,count10c=0,count5c=0;       
                          
         if (change>=20)  
         {        
             count20=((int)change)/20; 
             change=change-count20*20;     
         }                
         if (change>=10)   
         {         count10=((int)change)/10;     
         change=change-count10*10;  
         }      
         if (change>=5)     
         {     
             count5=((int)change)/5;  
             change=change-count5*5;     
         }     
         if (change>=1)   
         {         count1=((int)change)/1; 
         change=change-count1*1;    
         }         
         change=change*100;     
         if (change>=50)      
         {         
             count50c=((int)change)/50;     
         change=change-count50c*50;      
         }        
         if (change>=10)     
         {        
             count10c=((int)change)/10; 
             change=change-count10c*10;   
         }  
         if (change>=5)     
         {      
             count5c=((int)change)/5;  
             change=change-count5c*5;   
         } 
         System.out.println("Disbursed as : "+count20+" x N$20;  "+count10+" x N$10;  "+count5+" x N$5;  "+count1+" x N$1;  "+count50c+" x 50c;  "+count10c+" x 10c;  "+count5c+" x 5c; ");    
    } 
}  