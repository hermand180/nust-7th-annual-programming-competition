
package programmingquestion3;
import java.util.Scanner;// allows us to import the utility Scanner
public class ProgrammingQuestion3 {//name of the class

    
    public static void main(String[] args) {
        int males = 5, females = 3;      
        Scanner scan = new Scanner(System.in);    
        System.out.println("Welcome to NDF Captain's Selection Aid.");   
        System.out.println("***************************************");   
        System.out.print("Gender : ");       
        String gender = scan.next();      
        System.out.print("Mass in kilograms: ");   
        int mass = scan.nextInt();   
        System.out.print("Height in metres: ");   
        double height = scan.nextDouble();      
        System.out.print("Time for completing 10 km in minutes: ");     
        int mins = scan.nextInt();       
        double bmi = mass / (height*height);   
        if(gender.equalsIgnoreCase("male")) {         
            if(mins < 60) {         
                if(bmi > 18.5 && bmi < 25) {   
                    males += 1;               
                    System.out.println("Candidate Selected! The number of males is now " + males);  
                }             
                else if(bmi <= 18.5) {       
                    System.out.println("Candidate underweight! Recommend more food");    
                }                 else if(bmi >= 25 && bmi <= 30)      
                    System.out.println("Candidate Overweight. Recommend more training");      
                else if(bmi > 30)                
                    System.out.println("Candidate Obese. Reject!");        
            }      
                 
        }     
        else if(gender.equalsIgnoreCase("female")) {         
            if (mins < 80) {          
                if (bmi > 18.5 && bmi < 25) {      
                    females += 1;             
                    System.out.println("Candidate Selected! The number of females is now " + females);      
                } else if (bmi <= 18.5) {         
                    System.out.println("Candidate underweight! Recommend more food");       
                } else if (bmi >= 25 && bmi <= 30)        
                    System.out.println("Candidate Overweight. Recommend more training");  
                else if (bmi > 30)               
                    System.out.println("Candidate Obese. Reject!");      
            }      
        }         else        
            System.out.println("Invalid Gender");  
    }
}