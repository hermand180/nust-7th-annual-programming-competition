a = int(input("Enter first number: "))

# validating inputs for the first number
if a < 1 or a > 9:
    print("startNo cannot be less than 1 or greater than 9")
    quit()
b = int(input("Enter second number: "))

# validating inputs for the second number
if b < 1 or b > 9:
    print("endNo cannot be less than 1 or greater than 9")
    quit()
elif b < 0:
    print("endNo cannot be less than 0")

# addition operation
add = a+b
# making a comparison between a and b to make sure to return a positive answer from the subtraction
if a > b:
    sub = a-b
else:
    sub = b-a
# multiplication of the integers
multi = a*b

# division of the integers
division = a/b

# calculating the total of the results and their average
totals = multi+add+sub+division
num = 4
average = totals/num
print("Average is", average)
