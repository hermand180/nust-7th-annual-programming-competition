money = [200, 100, 50, 20, 10, 5, 1, 0.50, 0.10, 0.05]
my_list = []
print("Enter item name: ")
item = input()
print("Quantity of", item, ":")
quantity = int(input())
print("Enter price of said item", "N$: ")
price = int(input())
print("Amount tendered N$: ")
tendered = int(input())
cost = price*quantity
total_change = tendered-cost
print("Your change is N$:", total_change)

for x in str(total_change):
    my_list.append(int(x))

if total_change < 10:
    print("Disbursed as follows: 0 x N$20; 0 x N$10; 0 x N$5;", my_list[0], "x N$1; 0 × 50c; 0 x 10c; 0 × 5c")

elif 10 <= total_change < 100:
    print("Disbursed as follows: 0 x N$20;", my_list[0], " x N$10; 0 x N$5;", my_list[1], "x N$1; 0 × 50c; 0 x 10c; 0 × 5c")

elif total_change >= 100:
    print("Disbursed as follows:", my_list[0], "x 100;, 0 x 50; 0 x N$20;", my_list[1], " x N$10; 0 x N$5;", my_list[2], "x N$1; 0 × 50c; 0 x 10c; 0 × 5c")