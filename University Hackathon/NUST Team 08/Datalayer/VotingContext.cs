﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Nust_Vote_System.DatatabaseModel;

namespace Nust_Vote_System.Datalayer
{
    public class VotingContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public VotingContext(DbContextOptions<VotingContext> options)
     : base(options)
        {
        }



        public DbSet<Voters> Voters { get; set; }
        public DbSet<Candidates> Candidates { get; set; }
        public DbSet<StaffMember> StaffMember { get; set; }
        public DbSet<Student> Student { get; set; }
    }
}
