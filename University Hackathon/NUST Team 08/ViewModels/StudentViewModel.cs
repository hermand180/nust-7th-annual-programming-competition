﻿using System.ComponentModel.DataAnnotations;

namespace Nust_Vote_System.ViewModels
{
    public class StudentViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
    }
}
