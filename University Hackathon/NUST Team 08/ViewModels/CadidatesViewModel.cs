﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nust_Vote_System.ViewModels
{
    public class CadidatesViewModel
    {
        public int Id { get; set; }
        [DisplayName("Full Name")]
        public string FullName { get; set; }

        [DisplayName("Student Number")]
        public int StudentNumber { get; set; }
        [DisplayName("Study Mode ")]
        public string StudyMode { get; set; }
        [DisplayName("Position")]
        public string Position { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        [DisplayName("Image Name")]
        public string ImageName { get; set; }


        [NotMapped]
        [DisplayName("Upload File")]


        public IFormFile ImageFile { get; set; }
    }
}
