﻿using System.ComponentModel.DataAnnotations;

namespace Nust_Vote_System.Models
{
    public class VoteViewModel
    {
        [Required]
        public string Fname { get; set; }
        [Required]
        public string Lname { get; set; }
        [Required]
        public int StudentNumber { get; set; }
        [Required]
        public string StudyMode { get; set; }
    }
}
