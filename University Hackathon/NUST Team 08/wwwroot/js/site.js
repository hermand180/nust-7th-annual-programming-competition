﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.



////////////////////////////////////////////////////

//custom field to be used for required param validation(Saudai)
var isRequired = function () { throw new Error("param is required"); };
//gets a partial view from the server using provide string url and
//renders the partial views to the provided section of a page
//url: required string containing the URL to which the request is sent
//renderSectionId: required element ID where the returned view must be rendered to
//parameters: is option. Consist of parameters that are sent to server
function renderPartialView(url = isRequired(), renderSectionId = isRequired(), parameters = undefined) {
    //start page loader
    //enablePageLoader();
    console.log("In renderPartialView")

    if (!(parameters === "undefined") && parameters !== null) {
        //get the view
        return getData(url, parameters)
            .then(function (data) {
                $(".loader").css("display", "none")
                $(".loader-model-wrapper ").css("display", "none")
                //render partialview to provide section
                $("#" + renderSectionId).html(data);
                return data;
            }).catch(function (error) {
                $(".loader").css("display", "none")
                $(".loader-model-wrapper ").css("display", "none")
                throw new Error('Higher-level error. ', error + "\nMore details: " + Promise.reject(error));
            });
    }

    //get the view
    return getData(url).then(function (data) {
        //render partialview to provide section
        $("#" + renderSectionId).html(data);
        return data;
    }).catch(function (error) {
        throw new Error('Higher-level error. ', error + "\nMore details: " + Promise.reject(error));
    });
}

//a promise function of type 'GET' to query data from the server
//url: A string containing the URL to which the request is sent.(Saudai)
function getData(url = isRequired()) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: "GET"
        })
            .done(resolve)
            .fail(reject);
    });
}

//An overload function that gets data by parameters
//url: A string containing the URL to which the request is sent.
//data: information to send/post to server. (Saudai)
function getDataWithParam(url = isRequired(), parameters) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            data: parameters,
            type: "GET"
        })
            .done(resolve)
            .fail(reject);
    });
}

//a promise function to post data to server
//url: A string containing the URL to which the request is sent.
//data: information to send/post to server. (Saudai)
function postData(url = isRequired(), data = isRequired) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            data: data,
            type: "POST"
        })
            .done(resolve)
            .fail(reject);
    });
}

//a promise function to post multi part formdata to server
//url: A string containing the URL to which the request is sent.
//data: information to send/post to server. (Saudai)
function postFormData(url = isRequired(), data = isRequired) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            contentType: false,
            processData: false,
            data: data,
            type: "POST"
        })
            .done(resolve)
            .fail(reject);
    });
}
