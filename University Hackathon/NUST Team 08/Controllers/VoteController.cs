﻿using AspNetCoreHero.ToastNotification.Abstractions;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nust_Vote_System.Datalayer;
using Nust_Vote_System.DatatabaseModel;
using Nust_Vote_System.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nust_Vote_System.Controllers
{
    public class VoteController : Controller
    {
        private readonly VotingContext applicationDbContext;
        private readonly IMapper mapper;
        private readonly ILogger<AccountController> logger;
        private readonly INotyfService _notyf;
        private IWebHostEnvironment _Environment;

        public VoteController(IWebHostEnvironment Environment, INotyfService notyf, VotingContext applicationDbContext, IMapper mapper, ILogger<AccountController> logger)
        {
            this.applicationDbContext = applicationDbContext;
            this.mapper = mapper;
            this.logger = logger;
            _notyf = notyf;

            _Environment = Environment;
        }

        [HttpGet]
        public IActionResult Index()
        {

            return View();

        }

        public IActionResult Random()
        {

            return View();

        }

        public IActionResult Vote(int id)
        {

            var dispatch = new Candidates();

            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }

                dispatch = applicationDbContext.Candidates.FindAsync(id).Result;
                return View(dispatch);
            }
            catch (Exception e)
            {
                logger.LogError(e.Message);
                return BadRequest();
            }


        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _notyf.Custom("Deleted a Movie.", 5, "pink", "fas fa-trash-alt");

            var movie = await applicationDbContext.Candidates.FindAsync(id);
            applicationDbContext.Candidates.Remove(movie);
            //movieService.DeleteMovie(id);
            //await _movieHub.Clients.All.SendAsync("Create", Analytics());
            await applicationDbContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }


        public IActionResult President()
        {
            CadidatesViewModel movieView = new CadidatesViewModel();
            List<CadidatesViewModel> model = new List<CadidatesViewModel>();
            var rates = applicationDbContext.Candidates.ToList();

            var ratesModel = mapper.Map<IEnumerable<CadidatesViewModel>>(rates);

            CadidatesViewModelList detailsViewModel = new CadidatesViewModelList();


            detailsViewModel.Cadidates = new List<CadidatesViewModel>();


            detailsViewModel.Cadidates = (List<CadidatesViewModel>)ratesModel;

            return View(detailsViewModel);
        }
        public IActionResult VicePresident()
        {
            CadidatesViewModel movieView = new CadidatesViewModel();
            List<CadidatesViewModel> model = new List<CadidatesViewModel>();
            var rates = applicationDbContext.Candidates.ToList();

            var ratesModel = mapper.Map<IEnumerable<CadidatesViewModel>>(rates);

            CadidatesViewModelList detailsViewModel = new CadidatesViewModelList();


            detailsViewModel.Cadidates = new List<CadidatesViewModel>();


            detailsViewModel.Cadidates = (List<CadidatesViewModel>)ratesModel;

            return View(detailsViewModel);
        }


    }
}
