﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nust_Vote_System.Datalayer;
using Nust_Vote_System.ViewModels;
using System;
using System.Threading.Tasks;

namespace Nust_Vote_System.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly VotingContext applicationDbContext;
        private readonly IMapper mapper;
        private readonly ILogger<AccountController> logger;

        private readonly SignInManager<IdentityUser> _signInManager;

        public AccountController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, VotingContext applicationDbContext, IMapper mapper, ILogger<AccountController> logger)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            this.applicationDbContext = applicationDbContext;
            this.mapper = mapper;
            this.logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            ViewData["ReturnUrl"] = returnUrl;
            if (!User.Identity.IsAuthenticated)
            {
                return View("Login");
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _userManager.FindByNameAsync(model.UserName);


                    if (user == null)
                    {
                        ModelState.AddModelError("", "User not found in system.");
                        return BadRequest();
                    }

                    if (await _userManager.IsInRoleAsync(user, "Student") || await _userManager.IsInRoleAsync(user, "Staff") || await _userManager.IsInRoleAsync(user, "SuperAdmin"))
                    {
                        var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, lockoutOnFailure: false);
                        if (result.Succeeded)
                        {
                            logger.LogInformation("User logged in.");

                            return RedirectToAction("Index", "Home");

                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "User not found in system.");
            return BadRequest();
        }

        public async Task<IActionResult> Logout()
        {
            var user = HttpContext.User.Identity.Name;
            var userId = await _userManager.FindByNameAsync(user);
            await _signInManager.SignOutAsync();

            return RedirectToAction(nameof(Login), "Account");
        }

    }
}
