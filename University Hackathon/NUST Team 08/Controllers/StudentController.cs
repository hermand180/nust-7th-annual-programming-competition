﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nust_Vote_System.Datalayer;
using Nust_Vote_System.DatatabaseModel;
using Nust_Vote_System.ViewModels;
using System;
using System.Threading.Tasks;

namespace Nust_Vote_System.Controllers
{
    public class StudentController : Controller
    {
        private readonly VotingContext applicationDbContext;
        private readonly IMapper mapper;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<StudentController> logger;

        public StudentController(VotingContext applicationDbContext, IMapper mapper, UserManager<IdentityUser> userManager, ILogger<StudentController> logger)
        {
            this.applicationDbContext = applicationDbContext;
            this.mapper = mapper;
            _userManager = userManager;
            this.logger = logger;
        }






        [HttpPost]
        public async Task<IActionResult> AddStudentAsync(StudentViewModel driverViewModel)
        {
            //IsAlreadySigned(driverViewModel.Email);
            if (ModelState.IsValid)
            {
                try
                {
                    var user = new IdentityUser { UserName = driverViewModel.Email, Email = driverViewModel.Email, EmailConfirmed = true, PhoneNumber = driverViewModel.PhoneNumber, PhoneNumberConfirmed = true };
                    var emailExists = _userManager.FindByEmailAsync(user.Email).Result;

                    if (emailExists == null)
                    {
                        var result = await _userManager.CreateAsync(user, "Student@1234");
                        if (result.Succeeded)
                        {
                            await _userManager.AddToRoleAsync(user, "Driver");

                            var driver = mapper.Map<Student>(driverViewModel);

                            applicationDbContext.Student.Add(driver);
                            applicationDbContext.SaveChanges();

                            return RedirectToAction("Index");

                        }
                    }
                }
                catch (Exception e)
                {
                    logger.LogError(e.Message);
                    return BadRequest();
                }

            }
            return BadRequest();


        }



        // GET: StudentController
        public ActionResult Index()
        {
            return View();
        }

        // GET: StudentController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: StudentController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StudentController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: StudentController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: StudentController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: StudentController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: StudentController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
