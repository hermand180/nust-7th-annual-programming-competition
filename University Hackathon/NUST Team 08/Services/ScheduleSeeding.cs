﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace Nust_Vote_System.Services
{
    public class ScheduleSeeding
    {

        public static async Task<IdentityUser> CreateDefaultUsers(UserManager<IdentityUser> userManager)
        {
            var superAdmin = new IdentityUser { UserName = "SuperAdmin", Email = "superAdmin@sendit.com", EmailConfirmed = true };
            try
            {

                if (!(await IsUserExist(userManager, superAdmin)))
                {
                    await userManager.CreateAsync(superAdmin, "SuperAdmin@1234");
                    await userManager.AddToRoleAsync(superAdmin, "SuperAdmin");
                }

            }
            catch (Exception e)
            {

            }
            return superAdmin;
        }


        public static async Task<bool> IsUserExist(UserManager<IdentityUser> userManager, IdentityUser user)
        {
            try
            {
                var dbUser = await userManager.FindByNameAsync(user.UserName);

                return dbUser != null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
