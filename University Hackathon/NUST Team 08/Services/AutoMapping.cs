﻿using AutoMapper;
using Nust_Vote_System.DatatabaseModel;
using Nust_Vote_System.Models;
using Nust_Vote_System.ViewModels;

namespace Nust_Vote_System.Services
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<VoteViewModel, Voters>().ReverseMap();
            CreateMap<CadidatesViewModel, Candidates>().ReverseMap();
            CreateMap<StudentViewModel, Student>().ReverseMap();
        }
    }
}
