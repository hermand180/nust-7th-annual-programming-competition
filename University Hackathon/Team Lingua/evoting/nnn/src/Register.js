import React, { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link, useNavigate} from "react-router-dom";
import { db, storage } from "./firebase";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";

import {
  auth,
  registerWithEmailAndPassword,
  signInWithGoogle,
} from "./firebase";
import "./Register.css";
import "./index.css";
import { getFirestore } from "firebase/firestore";

function Register() {
  const [email, setEmail] = useState("");
  const [description, setDescription] = useState("");
  const [gender, setGender] = useState("");
  const [category, setCategory] = useState("");
  const [img, seTimg] = useState("");
  const [password, setPassword] = useState("");
  
  const [name, setName] = useState("");
  const navigate  = useNavigate();
  const register = () => {
   // if (!name) alert("Please enter name");
   // if (!photo) alert("Please enter Photo");
    //if (!description) alert("Please enter name");

    registerWithEmailAndPassword(name, email, password,gender,category,description);


    
    
}

const [image, setImage] = useState(null);
const [url, setUrl] = useState(null);

const handleImageChange = (e) => {
  if (e.target.files[0]) {
    setImage(e.target.files[0]);
  }
};

const handleSubmit = () => {
  const imageRef = ref(db, "image");
  uploadBytes(imageRef, image)
    .then(() => {
      getDownloadURL(imageRef)
        .then((url) => {
          setUrl(url);
        })
        .catch((error) => {
          console.log(error.message, "error getting the image url");
        });
      setImage(null);
    })
    .catch((error) => {
      console.log(error.message);
    });
};

  return (
    <div className="register left-40">
      <div className="register__container">
        <input
          type="text"
          className="register__textBox"
          value={name}
          onChange={(e) => setName(e.target.value)}
          placeholder="Full Name"
        />
        <label for="cars">Choose a Category:</label>

<select value={category}
          onChange={(e) => setCategory(e.target.value)}>
  <option value="volvo">Student</option>
  <option value="saab">Saab</option>
  <option value="mercedes">Mercedes</option>
  <option value="audi">Audi</option>
</select>

<label for="cars">Choose a Gender:</label>

<select  value={category}
          onChange={(e) => setCategory(e.target.value)}>
  <option text="malse">Male</option>
  <option 
         >Female</option>
</select>
        <input
          type="text"
          className="register__textBox"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="E-mail Address"
        />
        <label for="cars">Choose a Photo:</label>
      <input type="file"
                
       accept="image/png, image/jpeg"/>
        <input
          type="password"
          className="register__textBox"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Password"
        />
        <textarea  value={description}
                 onChange={(e) => setDescription(e.target.value)}></textarea>
        <button className="register__btn" onClick={register}>
          Register
        </button>
        
        <button
          className="register__btn register__google"
          onClick={signInWithGoogle}
        >
          Register with Google
        </button>
        <div>
          Already have an account? <Link to="/">Login</Link> now.
        </div>
      </div>
      <div className="App">
      <input type="file" onChange={handleImageChange} />
      <button onClick={handleSubmit}>Submit</button>
    </div>
    </div>
  );
}
export default Register;