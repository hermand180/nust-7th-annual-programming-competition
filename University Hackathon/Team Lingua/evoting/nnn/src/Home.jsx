import React from "react";
import "./index.css"


function Home() {
    return (
      
      <>
      <div className="block">
<div class="w-80 top-20 absolute text-sm text-center text-1xl font-medium text-gray-900 bg-white  border border-gray-200 dark:bg-gray-700 dark:border-gray-600 dark:text-white ">
    <div className="h- text-center"><h1 className="text-5xl py-10   text-blue-700">Welcome</h1></div>
    <a href="#" aria-current="true" class="  block py-5 px-4 w-full text-white bg-blue-700  border-b border-gray-200 cursor-pointer dark:bg-gray-800 dark:border-gray-600">
    President
    </a>
    <a href="#" class="block -2  px-4 w-full border-b py-5 border-gray-200 cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white">
    Vice President
    </a>
    <a href="#" class="block px-4 w-full border-b py-5 border-gray-200 cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white">
    Secretary of Finance
    </a>
    <a href="#" class="block  px-4 w-full border-b   cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white">
    Secretary of gender, health and development
    </a>
    <a href="#" class="block py-5 px-4 w-full border-b  cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white">
    Secretary General
    </a>
    <a href="#" class="block py-5 px-4 w-full border-b    cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white">
    Secretary of internal affairs
    </a>
    <a href="#" class="block py-5 px-4 w-full border-b    cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white">
    Secretary pf academic affairs
    </a>
    <a href="#" class="block  px-4 w-full border-b   py-5 cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white">
    Secretary of external affairs
    </a>
    <a href="#" class="block py-5 px-4 w-full border-b cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white">
    Secretary information and publicity
      
    </a>
    <a href="#" class="block  px-4 w-full border-b   py-5 cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white">
    Secretary of sports, recreation and culture
    </a>
    <a href="#" class="block   py-5 w-full border-b  cursor-pointer hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white">
    Secretary of accomodation
    </a>
    <input  class="bg-green-600 py-5  text-white font-bold  px-4 w-80 hover:bg-gray-100 hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500 dark:focus:text-white" type="submit" id="button" value="Vote and Save" />

</div>
<div className="px-96
">

  <div className="bg-gray-50 w-96 h-96 rounded">     <img className="rounded-xl" src="img/plain-dark-blue-wooden-wall-product-background.jpg"/></div>
</div>
</div>

 

      </>
      
      
    );
  }
  
  export default Home;
  