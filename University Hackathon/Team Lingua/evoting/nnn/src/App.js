import "./index.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Navbar from "./Nav";
import Home from "./Home";

function App() {
  return (
    <div className="app">
      
      <Navbar/>
      <Home/>
      
    </div>
  );
}
export default App;