import { initializeApp } from "firebase/app";
import {
  GoogleAuthProvider,
  getAuth,
  signInWithPopup,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  sendPasswordResetEmail,
  signOut,
} from "firebase/auth";

import {
  getFirestore,
  query,
  getDocs,
  collection,
  where,
  addDoc,
  Firestore,
} from "firebase/firestore";

import { getStorage } from "firebase/storage";
const firebaseConfig = {
  apiKey: "AIzaSyAavXUXgSW_zplIHbAz9UTDSaI6ZMAFH-E",
  authDomain: "voting-da8aa.firebaseapp.com",
  projectId: "voting-da8aa",
  storageBucket: "voting-da8aa.appspot.com",
  messagingSenderId: "775045284257",
  appId: "1:775045284257:web:c38388fe7c9850c4210be0"
};
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);
const data = getFirestore(app);
const googleProvider = new GoogleAuthProvider();


const signInWithGoogle = async () => {
  try {
    const res = await signInWithPopup(auth, googleProvider);
    const user = res.user;
    const storage = getStorage(app);
    const q = query(collection(db, "user"), where("uid", "==", user.uid));
    const docs = await getDocs(q);
    if (docs.docs.length === 0) {
      await addDoc(collection(db, "user"), {
        uid: user.uid,
        name: user.displayName,
        authProvider: "google",
        email: user.email,
        
      });
    }
  } catch (err) {
    console.error(err);
    alert(err.message);
  }
};




const logInWithEmailAndPassword = async (email, password) => {
  try {
    await signInWithEmailAndPassword(auth, email, password);
  } catch (err) {
    console.error(err);
    alert(err.message);
  }
};


const registerWithEmailAndPassword = async (name, email, password,gender,category,description,check) => {
  try {
    
    const res = await createUserWithEmailAndPassword(auth, email, password,gender,category,check,description);
    
    const user = res.user;
    
        await addDoc(collection(db, "users"), {
      uid: user.uid,
      name,
      authProvider: "local",
      email,
      gender,
      category,
      check: "no",
      description

      
      
    })
    
    
    ;
  } catch (err) {
    console.error(err);
    alert(err.message);
  }
};


const sendPasswordReset = async (email) => {
  try {
    await sendPasswordResetEmail(auth, email);
    alert("Password reset link sent!");
  } catch (err) {
    console.error(err);
    alert(err.message);
  }
};
const logout = () => {
  signOut(auth);
};
export const storage = getStorage(app);
export 
{
  auth,
  db,
  signInWithGoogle,
  logInWithEmailAndPassword,
  registerWithEmailAndPassword,
  sendPasswordReset,
  logout,
  signInWithEmailAndPassword
  
  
  
};