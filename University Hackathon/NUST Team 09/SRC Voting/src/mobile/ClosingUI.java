package mobile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClosingUI implements ActionListener {
    private static JWindow window;
    private static JPanel panel;
    private static JLabel label;
    private static JButton button;

    static void Done() {
        window = new JWindow();
        window.setSize(300,120);
        window.setLocation(500,200);


        panel = new JPanel();
        panel.setLayout(null);
        window.add(panel);

        label = new JLabel("Thank You For Your Vote!!");
        label.setBounds(70,20,200,25);
        panel.add(label);

        button = new JButton("CLOSE");
        button.setBounds(90,85,100,20);
        button.addActionListener(new ClosingUI());
        panel.add(button);


        window.setVisible(true);

    }

    public void actionPerformed(ActionEvent e) {
        window.setVisible(false);
        VerificationUI.main1();

    }




}
