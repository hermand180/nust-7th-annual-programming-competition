package mobile;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CandidateVoting implements ListSelectionListener {
    private static JWindow window;
    private static JPanel panel;
    private static JLabel label1, label2, label3;
    private static JList list;
    private static JButton button;
    private static ButtonGroup buttonGroup;

    static void president(){
        window = new JWindow();
        window.setLocation(500,200);
        window.setSize(400,350);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setBorder(BorderFactory.createEmptyBorder(50,10,100,10));
        panel.setLayout(new BorderLayout());
        window.add(panel);

        buttonGroup = new ButtonGroup();

        label1 =new JLabel("PRESIDENT CANDIDATES");
        label1.setBounds(100,10,270,25);
        panel.add(label1);

        String[] candidates = {"Martha Ambabi","Sakaria Elago", "BACK..."};

        list = new JList(candidates);
        list.setBounds(10,50,100,30);
        list.setFixedCellHeight(40);
        list.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        list.setFixedCellWidth(100);
        list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (list.getSelectedValue()== candidates[0]){
                    window.dispose();
                    DatabaseUpdate.databaseUpdate(0);
                }else if(list.getSelectedValue()== candidates[1]){
                    window.dispose();
                    CandidateVoting.vicePresident();
                }else if(list.getSelectedValue()== candidates[2]){
                    window.dispose();
                    PortifoliosUI.portifolios();
                }
            }
        });
        panel.add(list);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        list.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);

//        button = new JButton("Cancel");
//        button.setBounds(90,85,100,20);
//        button.setSize(100,20);
//        window.add(button);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                window.dispose();
//                PortifoliosUI.portifolios();
//            }
//        });
        window.setVisible(true);
    }
    static void vicePresident(){
        window = new JWindow();
        window.setLocation(500,200);
        window.setSize(400,350);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setBorder(BorderFactory.createEmptyBorder(50,10,50,10));
        panel.setLayout(new BorderLayout());
        window.add(panel);

        buttonGroup = new ButtonGroup();

        label1 =new JLabel("VICE PRESIDENT CANDIDATES");
        label1.setBounds(100,10,270,25);
        panel.add(label1);

        String[] candidates = {"Kumbee Tjirongo","LIkius Katengele","Charity Doeses"};

        list = new JList(candidates);
        list.setBounds(10,50,100,30);
        list.setFixedCellHeight(40);
        list.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        list.setFixedCellWidth(100);
        list.addListSelectionListener(new PortifoliosUI());
        panel.add(list);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        list.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);

//        button = new JButton("Cancel");
//        button.setBounds(240,300,100,25);
//        window.add(button);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                window.dispose();
//                MenuUI.mainMenu();
//            }
//        });
        window.setVisible(true);
    }
    static void secretaryGeneral(){
        window = new JWindow();
        window.setLocation(500,200);
        window.setSize(400,350);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setBorder(BorderFactory.createEmptyBorder(50,10,50,10));
        panel.setLayout(new BorderLayout());
        window.add(panel);

        buttonGroup = new ButtonGroup();

        label1 =new JLabel("SECRETARY GENERAL CANDIDATES");
        label1.setBounds(100,10,270,25);
        panel.add(label1);

        String[] candidates = {"Tarohole \"Tara\" Niipare","Claudine Gaoses"};

        list = new JList(candidates);
        list.setBounds(10,50,100,30);
        list.setFixedCellHeight(40);
        list.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        list.setFixedCellWidth(100);
        list.addListSelectionListener(new PortifoliosUI());
        panel.add(list);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        list.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);

//        button = new JButton("Cancel");
//        button.setBounds(240,300,100,25);
//        window.add(button);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                window.dispose();
//                MenuUI.mainMenu();
//            }
//        });
        window.setVisible(true);
    }
    static void finance(){
        window = new JWindow();
        window.setLocation(500,200);
        window.setSize(400,350);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setBorder(BorderFactory.createEmptyBorder(50,10,50,10));
        panel.setLayout(new BorderLayout());
        window.add(panel);

        buttonGroup = new ButtonGroup();

        label1 =new JLabel("FINANCE CANDIDATES");
        label1.setBounds(100,10,270,25);
        panel.add(label1);

        String[] candidates = {"Brian-Lissa Mulundu","There are no other candidates..."};

        list = new JList(candidates);
        list.setBounds(10,50,100,30);
        list.setFixedCellHeight(40);
        list.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        list.setFixedCellWidth(100);
        list.addListSelectionListener(new PortifoliosUI());
        panel.add(list);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        list.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);

//        button = new JButton("Cancel");
//        button.setBounds(240,300,100,25);
//        window.add(button);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                window.dispose();
//                MenuUI.mainMenu();
//            }
//        });
        window.setVisible(true);
    }
    static void internalAffairs(){
        window = new JWindow();
        window.setLocation(500,200);
        window.setSize(400,350);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setBorder(BorderFactory.createEmptyBorder(50,10,50,10));
        panel.setLayout(new BorderLayout());
        window.add(panel);

        buttonGroup = new ButtonGroup();

        label1 =new JLabel("INTERNAL AFFAIRS CANDIDATES");
        label1.setBounds(100,10,270,25);
        panel.add(label1);

        String[] candidates = {"Antoniette Simeon","Helena Mweshitale", "Nguno Nahambo"};

        list = new JList(candidates);
        list.setBounds(10,50,100,30);
        list.setFixedCellHeight(40);
        list.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        list.setFixedCellWidth(100);
        list.addListSelectionListener(new PortifoliosUI());
        panel.add(list);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        list.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);

//        button = new JButton("Cancel");
//        button.setBounds(240,300,100,25);
//        window.add(button);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                window.dispose();
//                MenuUI.mainMenu();
//            }
//        });
        window.setVisible(true);
    }
    static void academicAffairs(){
        window = new JWindow();
        window.setLocation(500,200);
        window.setSize(400,350);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setBorder(BorderFactory.createEmptyBorder(50,10,50,10));
        panel.setLayout(new BorderLayout());
        window.add(panel);

        buttonGroup = new ButtonGroup();

        label1 =new JLabel("ACADEMIC AFFAIRS CANDIDATES");
        label1.setBounds(100,10,270,25);
        panel.add(label1);

        String[] candidates = {"Michee Masengo","Naameandia Williams"};

        list = new JList(candidates);
        list.setBounds(10,50,100,30);
        list.setFixedCellHeight(40);
        list.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        list.setFixedCellWidth(100);
        list.addListSelectionListener(new PortifoliosUI());
        panel.add(list);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        list.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);

//        button = new JButton("Cancel");
//        button.setBounds(240,300,100,25);
//        window.add(button);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                window.dispose();
//                MenuUI.mainMenu();
//            }
//        });
        window.setVisible(true);
    }
    static void accommodation(){
        window = new JWindow();
        window.setLocation(500,200);
        window.setSize(400,350);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setBorder(BorderFactory.createEmptyBorder(50,10,50,10));
        panel.setLayout(new BorderLayout());
        window.add(panel);

        buttonGroup = new ButtonGroup();

        label1 =new JLabel("ACCOMMODATION CANDIDATES");
        label1.setBounds(100,10,270,25);
        panel.add(label1);

        String[] candidates = {"Diina Kanana","There are no other candidates..."};

        list = new JList(candidates);
        list.setBounds(10,50,100,30);
        list.setFixedCellHeight(40);
        list.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        list.setFixedCellWidth(100);
        list.addListSelectionListener(new PortifoliosUI());
        panel.add(list);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        list.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);

//        button = new JButton("Cancel");
//        button.setBounds(240,300,100,25);
//        window.add(button);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                window.dispose();
//                MenuUI.mainMenu();
//            }
//        });
        window.setVisible(true);
    }
    static void informationAndPublicity(){
        window = new JWindow();
        window.setLocation(500,200);
        window.setSize(400,350);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setBorder(BorderFactory.createEmptyBorder(50,10,50,10));
        panel.setLayout(new BorderLayout());
        window.add(panel);

        buttonGroup = new ButtonGroup();

        label1 =new JLabel("INFORMATION AND PUBLICITY CANDIDATES");
        label1.setBounds(100,10,270,25);
        panel.add(label1);

        String[] candidates = {"Iyaloo \"Blinnk\" Nghandi", "Isdor Kamati","Benson Ntemo"};

        list = new JList(candidates);
        list.setBounds(10,50,100,30);
        list.setFixedCellHeight(40);
        list.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        list.setFixedCellWidth(100);
        list.addListSelectionListener(new PortifoliosUI());
        panel.add(list);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        list.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);

//        button = new JButton("Cancel");
//        button.setBounds(240,300,100,25);
//        window.add(button);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                window.dispose();
//                MenuUI.mainMenu();
//            }
//        });
        window.setVisible(true);
    }
    static void externalAffairsAndLiaison(){
        window = new JWindow();
        window.setLocation(500,200);
        window.setSize(400,350);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setBorder(BorderFactory.createEmptyBorder(50,10,50,10));
        panel.setLayout(new BorderLayout());
        window.add(panel);

        buttonGroup = new ButtonGroup();

        label1 =new JLabel("EXTERNAL AFFAIRS AND LIAISON");
        label1.setBounds(100,10,270,25);
        panel.add(label1);

        String[] candidates = {"Tayaa Shivolo","Mirjam Ndengu","Thandeka Sibanda", "Rubain Kalombo"};

        list = new JList(candidates);
        list.setBounds(10,50,100,30);
        list.setFixedCellHeight(40);
        list.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        list.setFixedCellWidth(100);
        list.addListSelectionListener(new PortifoliosUI());
        panel.add(list);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        list.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);

//        button = new JButton("Cancel");
//        button.setBounds(240,300,100,25);
//        window.add(button);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                window.dispose();
//                MenuUI.mainMenu();
//            }
//        });
        window.setVisible(true);
    }
    static void sportsRecreationAndCulture(){
        window = new JWindow();
        window.setLocation(500,200);
        window.setSize(400,350);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setBorder(BorderFactory.createEmptyBorder(50,10,50,10));
        panel.setLayout(new BorderLayout());
        window.add(panel);

        buttonGroup = new ButtonGroup();

        label1 =new JLabel("SPORTS, RECREATION AND CULTURE CANDIDATES");
        label1.setBounds(100,10,270,25);
        panel.add(label1);

        String[] candidates = {"Hanganeni Fikunawa","Chantelle Van Wyk"};

        list = new JList(candidates);
        list.setBounds(10,50,100,30);
        list.setFixedCellHeight(40);
        list.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        list.setFixedCellWidth(100);
        list.addListSelectionListener(new PortifoliosUI());
        panel.add(list);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        list.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);

//        button = new JButton("Cancel");
//        button.setBounds(240,300,100,25);
//        window.add(button);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                window.dispose();
//                MenuUI.mainMenu();
//            }
//        });
        window.setVisible(true);
    }
    static void genderHealthAndDevelopment(){
        window = new JWindow();
        window.setLocation(500,200);
        window.setSize(400,350);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setBorder(BorderFactory.createEmptyBorder(50,10,50,10));
        panel.setLayout(new BorderLayout());
        window.add(panel);

        buttonGroup = new ButtonGroup();

        label1 =new JLabel("GENDER, HEALTH AND DEVELOPMENT CANDIDATES");
        label1.setBounds(100,10,270,25);
        panel.add(label1);

        String[] candidates = {"Desiree Booysen","Charlene Makwaza"};

        list = new JList(candidates);
        list.setBounds(10,50,100,30);
        list.setFixedCellHeight(40);
        list.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        list.setFixedCellWidth(100);
        list.addListSelectionListener(new PortifoliosUI());
        panel.add(list);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(list);
        list.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);

//        button = new JButton("Cancel");
//        button.setBounds(240,300,100,25);
//        window.add(button);
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                window.dispose();
//                MenuUI.mainMenu();
//            }
//        });
        window.setVisible(true);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}
