package mobile;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class VerificationUI implements ActionListener{
    private static JLabel userLabel;
    private static JTextField userText;
    private static JLabel passwordLabel;
    private static JPasswordField passwordText;
    private static JButton button;
    private static JLabel success;
    private static JFrame frame;


    public static void main1() {
        JPanel panel = new JPanel();
        frame = new JFrame();
        frame.setSize(380, 200);
        frame.setLocation(500,200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.add(panel);

        panel.setLayout(null);

        userLabel = new JLabel("Student ID:");
        userLabel.setBounds(10, 20, 80, 25);
        panel.add(userLabel);

        userText = new JTextField();
        userText.setBounds(100, 20, 165, 25);
        panel.add(userText);

        passwordLabel = new JLabel("Password:");
        passwordLabel.setBounds(10, 50, 80, 25);
        panel.add(passwordLabel);

        passwordText = new JPasswordField();
        passwordText.setBounds(100, 50, 165, 25);
        panel.add(passwordText);

        button = new JButton("SIGN IN");
        button.setBounds(10, 80, 80, 25);
        button.addActionListener(new VerificationUI());
        panel.add(button);


        success = new JLabel();
        success.setBounds(10, 110, 350, 25);
        panel.add(success);


        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String id = userText.getText();
        String password = String.valueOf(passwordText.getPassword());
        int idLength = id.length();
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                final String DB_URL = "jdbc:mysql://sql5.freesqldatabase.com:3306/sql5528311";
                final String USERNAME = "sql5528311";
                final String PASSWORD = "YN1rl582Tm";
                Connection connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
                String sqlCommand = "SELECT * FROM RegisteredStudents WHERE StudentNo=? AND Password=?";
                PreparedStatement pst = connection.prepareStatement(sqlCommand);
                pst.setString(1,id);
                pst.setString(2,password);
                ResultSet rs = pst.executeQuery();
                if (rs.next()){
                    success.setText("Welcome...");
                    frame.dispose();
                   WelcomeUI.welcome();
                }else{
                    success.setText("Incorrect Student ID or Password!");
                    userText.setText("");
                    passwordText.setText("");
                }
                connection.close();
            }catch (SQLException exception){

//                JOptionPane.showMessageDialog(null,"Please Correctly fill in your Student ID and Password!");
                success.setText("Please Correctly fill in your Student ID and Password!");
                userText.setText("");
                passwordText.setText("");


            } catch (ClassNotFoundException ex) {
                throw new RuntimeException(ex);
            }

//        String user = userText.getText();
//        String password = passwordText.getText();
//        int idLength = user.length();
//        if ((parseInt(password ) <= 2004) && (idLength == 11 )) {
//            success.setText("You are ELIGIBLE to VOTE.");
//
//            MenuUI.mainMenu();
//            frame.setVisible(false);
//        } else{
//            success.setText("Please Correctly fill in your Student ID and Password!");
//        }



    }
}
