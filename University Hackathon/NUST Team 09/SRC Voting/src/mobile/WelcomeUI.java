package mobile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WelcomeUI extends JFrame implements ActionListener {
    private static JWindow window;
    private static JPanel panel;
    private static JLabel label, label1;
    private static JButton button;

    static void welcome() {
        window = new JWindow();
        window.setSize(500,160);
        window.setLocation(500,200);


        panel = new JPanel();
        panel.setLayout(null);
        window.add(panel);

        label = new JLabel("Welcome..." );
        label.setBounds(10,30,500,25);
        panel.add(label);

        label1 = new JLabel("NOTE: YOU ARE ONLY ALLOWED TO VOTE FOR ONE CANDIDATE PER PORTIFOLIO");
        label1.setBounds(10,60,500,30);
        panel.add(label1);


        button = new JButton("NEXT");
        button.setBounds(350,120,100,20);
        button.addActionListener(new WelcomeUI());
        panel.add(button);


        window.setVisible(true);

    }

    public void actionPerformed(ActionEvent e) {
        window.setVisible(false);
        PortifoliosUI.portifolios();

    }



}

