package mobile;

import java.awt.*;
import java.util.Arrays;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class PortifoliosUI implements ListSelectionListener {

    private static JFrame frame;
    private static JList jList;
    private static JPanel panel;
    private static JLabel label;
    private static String[] departmentNames = {"PRESIDENT","VICE PRESIDENT","SECRETARY GENERAL","FINANCE", "INTERNAL AFFAIRS","ACADEMIC AFFAIRS",
            "ACCOMMODATION", "INFORMATION AND PUBLICITY","EXTERNAL AFFAIRS AND LIAISON", "SPORTS, RECREATION AND CULTURE","GENDER, HEALTH AND DEVELOPMENT"};
    static void portifolios(){

        frame = new JFrame();
        frame.setTitle("PORTIFOLIOS");
        frame.setSize(400,300);
        frame.setResizable(false);
        frame.setLocation(500,200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setBounds(10,30,400,500);
        panel.setLayout(new BorderLayout());
        frame.add(panel);


        jList =  new JList(departmentNames);
        jList.setBounds(10,50,100,30);
        jList.setFixedCellHeight(30);
        jList.setFixedCellWidth(100);
        jList.addListSelectionListener(new PortifoliosUI());
        panel.add(jList);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(jList);
        jList.setLayoutOrientation(JList.VERTICAL);
        panel.add(scrollPane);


        frame.show();


    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (jList.getSelectedValue()== departmentNames[0]){
            frame.dispose();
            CandidateVoting.president();
        }else if(jList.getSelectedValue()== departmentNames[1]){
            frame.dispose();
            CandidateVoting.vicePresident();
        }else if(jList.getSelectedValue()== departmentNames[2]){
            frame.dispose();
            CandidateVoting.secretaryGeneral();
        }else if(jList.getSelectedValue()== departmentNames[3]){
            frame.dispose();
            CandidateVoting.finance();
        }else if(jList.getSelectedValue()== departmentNames[4]){
            frame.dispose();
            CandidateVoting.internalAffairs();
        }else if(jList.getSelectedValue()== departmentNames[5]){
            frame.dispose();
            CandidateVoting.academicAffairs();
        }else if(jList.getSelectedValue()== departmentNames[6]){
            frame.dispose();
            CandidateVoting.accommodation();
        }else if(jList.getSelectedValue()== departmentNames[7]){
            frame.dispose();
            CandidateVoting.informationAndPublicity();
        }else if(jList.getSelectedValue()== departmentNames[8]){
            frame.dispose();
            CandidateVoting.externalAffairsAndLiaison();
        }else if(jList.getSelectedValue()== departmentNames[9]){
            frame.dispose();
            CandidateVoting.sportsRecreationAndCulture();
        }else if(jList.getSelectedValue()== departmentNames[10]){
            frame.dispose();
            CandidateVoting.genderHealthAndDevelopment();
        }
    }
}