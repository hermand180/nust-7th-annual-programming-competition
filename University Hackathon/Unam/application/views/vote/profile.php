
<?php
          $SessionId = $this->session->userdata('id');
          if(empty($SessionId))
          {
            redirect(base_url('welcome/vote'));
          }
          // $resultSet =  $this->db->query("SELECT * FROM tbl_admin WHERE id = '$SessionId' "); 
          // $result = $resultSet->result_array();
          // $Image = $result[0]['image'];
          // $Username = $result[0]['username'];
          // $email = $result[0]['email'];
          // $password = $result[0]['password']; 
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<style type="text/css">
		body{
    color: #6F8BA4;
    margin-top:20px;
}
.section {
    padding: 100px 0;
    position: relative;
}
.gray-bg {
    background-color: #f5f5f5;
}
img {
    max-width: 100%;
}
img {
    vertical-align: middle;
    border-style: none;
}
/* About Me 
---------------------*/
.about-text h3 {
  font-size: 45px;
  font-weight: 700;
  margin: 0 0 6px;
}
@media (max-width: 767px) {
  .about-text h3 {
    font-size: 35px;
  }
}
.about-text h6 {
  font-weight: 600;
  margin-bottom: 15px;
}
@media (max-width: 767px) {
  .about-text h6 {
    font-size: 18px;
  }
}
.about-text p {
  font-size: 18px;
  max-width: 450px;
}
.about-text p mark {
  font-weight: 600;
  color: #20247b;
}

.about-list {
  padding-top: 10px;
}
.about-list .media {
  padding: 5px 0;
}
.about-list label {
  color: #20247b;
  font-weight: 600;
  width: 88px;
  margin: 0;
  position: relative;
}
.about-list label:after {
  content: "";
  position: absolute;
  top: 0;
  bottom: 0;
  right: 11px;
  width: 1px;
  height: 12px;
  background: #20247b;
  -moz-transform: rotate(15deg);
  -o-transform: rotate(15deg);
  -ms-transform: rotate(15deg);
  -webkit-transform: rotate(15deg);
  transform: rotate(15deg);
  margin: auto;
  opacity: 0.5;
}
.about-list p {
  margin: 0;
  font-size: 15px;
}

@media (max-width: 991px) {
  .about-avatar {
    margin-top: 30px;
  }
}

.about-section .counter {
  padding: 22px 20px;
  background: #ffffff;
  border-radius: 10px;
  box-shadow: 0 0 30px rgba(31, 45, 61, 0.125);
}
.about-section .counter .count-data {
  margin-top: 10px;
  margin-bottom: 10px;
}
.about-section .counter .count {
  font-weight: 700;
  color: #20247b;
  margin: 0 0 5px;
}
.about-section .counter p {
  font-weight: 600;
  margin: 0;
}
mark {
    background-image: linear-gradient(rgba(252, 83, 86, 0.6), rgba(252, 83, 86, 0.6));
    background-size: 100% 3px;
    background-repeat: no-repeat;
    background-position: 0 bottom;
    background-color: transparent;
    padding: 0;
    color: currentColor;
}
.theme-color {
    color: #fc5356;
}
.dark-color {
    color: #20247b;
}


	</style>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<title></title>
</head>
<body>

<?php $id=$this->session->userdata('identity'); ?>

<section class="section about-section gray-bg" id="about">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-lg-6">
                        <div class="about-text go-to">  <?php foreach ($res as $key => $val) {?>  
                            <h3 class="dark-color"><?php echo strtoupper ($val['name'])  ?></h3>
                            <h6 class="theme-color lead">Welcome To &amp; NUST Sudent e-Voting System</h6>
                            <!-- <p>I <mark>design and develop</mark> services for customers of all sizes, specializing in creating stylish, modern websites, web services and online stores. My passion is to design digital user experiences through the bold interface and meaningful interactions.</p> -->
                            <div class="row about-list">
                                <div class="col-md-6">
                                    <div class="media">
                                        <label>Date Of Birth</label>
                                        <p><?php echo $val['dob']  ?></p>
                                    </div>
                                    <!-- <div class="media">
                                        <label>Age</label>
                                        <p>22 Yr</p>
                                    </div> -->
                                    <div class="media">
                                        <label>Gender</label>
                                        <p><?php echo $val['email']  ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Address</label>
                                        <p><?php echo $val['website']  ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="media">
                                        <label>E-mail</label>
                                        <p>nuststudents@nust.edu.na</p>
                                    </div>
                                    <div class="media">
                                        <label>Password</label>
                                        <p><?php echo $val['identity'] ?></p>
                                    </div>
                                    <!-- <div class="media">
                                        <label>Skype</label>
                                        <p>skype.0404</p>
                                    </div> -->
                                    <!-- <div class="media">
                                        <label>Freelance</label>
                                        <p>Available</p>
                                    </div> -->
                                </div>
                            </div>
                       
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-avatar">
                            <img src="<?=base_url('upload/'.$val['logo'])?> " title="" alt="" height="315" width="315">
                        </div>
                    </div><?php } ?>
                </div><form action="<?php echo base_url('welcome/save_vote') ?>" method = 'post'>
                <div class="counter">
                    <div class="row">
                    <?php foreach ($result as $key => $value) {?>
                       
               
                        <div class="col-6 col-lg-3">
                            <div class="count-data text-center">
                                <h6 class="count h2" data-to="500" data-speed="500"><?=$value['first_name']?></h6>
                                <input type="hidden" name="identity" value="<?php echo $id ?>" >
                                 <p class="m-0px font-w-600"><img src="<?=base_url('logo/'.$value['last_name'])?>"></p>
                                 <p class="m-0px font-w-600"> <input type="checkbox" class="Present 1" name="vote[]" id ="1"  value="<?=$value['first_name']?>" required></p>
                            </div>
                        </div>   <?php  } ?> 
                       <!--  <div class="col-6 col-lg-3">
                            <div class="count-data text-center">
                                <h6 class="count h2" data-to="150" data-speed="150">CONGRESS</h6>
                                <p class="m-0px font-w-600"><img src="<?php echo site_url().'logo/download.png'  ?> "></p>
                                <p class="m-0px font-w-600"> <input type="checkbox" class="Present 1" name="vote[]" id ="1" value="con" required></p>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3">
                            <div class="count-data text-center">
                                <h6 class="count h2" data-to="850" data-speed="850">AAP</h6>
                                 <p class="m-0px font-w-600"><img src="<?php echo site_url().'logo/images.jpg'  ?> "></p>
                                <p class="m-0px font-w-600"> <input type="checkbox" class="Present 1" name="vote[]" id ="1"  value="aap" required></p>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3">
                            <div class="count-data text-center">
                                <h6 class="count h2" data-to="190" data-speed="190">NOTA</h6>
                                 <p class="m-0px font-w-600"><img src="<?php echo site_url().'logo/nota.png'  ?> "></p>
                                <p class="m-0px font-w-600"><input type="checkbox" class="Present 1" name="vote[]" id ="1"  value="nota" required></p>
                            </div>
                        </div> -->


                       <button type="submit" class="btn btn-primary btn-lg btn-block">Vote</button>
                    </div>
                </div></form>
            </div>
        </section>
</body>
</html>

<script type="text/javascript">

  $('input:checkbox').click(function(){

   var id = ($(this).attr('id'));

   // alert(id)

    var $inputs = $("#id")

        if($(this).is(':checked')){

            // $("."+id).prop('disabled', true); // <-- disable all but checked one

             $("."+id).not(this).prop('disabled',true);

        }else{

           // $inputs.prop('disabled',false); // <--

           $("."+id).prop('disabled', false);

        }

    })

</script>