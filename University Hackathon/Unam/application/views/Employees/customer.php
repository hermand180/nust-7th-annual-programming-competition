         

<div class="right_col" role="main">
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
 <a href="<?=base_url('welcome/add_employee')?>">
  <button type="button" class="btn btn-success">Add Candidate</button></a>
        <div class="clearfix"></div>
      </div>


                  <div class="x_content">
                    <div class="grid-body ">
            <div class="col-sm-12" style="margin-bottom:25px;">
                <?php 
                    if(!$this->session->flashdata('msg')==''){  ?>
                       <center>   
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>
                                <h4>  <?php  echo $this->session->flashdata('msg'); ?></h4>
                            </div>
                        </center>
                <?php } ?>

                <?php 
                if(!$this->session->flashdata('error_msg')==''){  ?>
                    <center>   
                        <div class="alert alert-danger alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>
                            <h4>  <?php  echo $this->session->flashdata('error_msg'); ?></h4>
                        </div>
                    </center>
                <?php } ?>
            </div> 
        </div>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                            <th>Candidate Name</th>
                            <th>Logo</th>
                         
                        
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
<?php 
foreach ($result as $value) {
  $id=$value['id'];

  $c=$value['Companies'];

?>

                        <tr>
                          <td><?=$value['first_name']; ?></td>
                          <?php $image=$value['last_name']; ?>
                          <td> <img src="<?=site_url('logo/'.$image)?>" height="40px" width="40px"></td>
                    
                           <td> <a href="<?=base_url('welcome/edit_employee/'.$id)?>">
  <button type="button" class="btn btn-primary">Edit</button></a>
 
<a href="<?=base_url('welcome/delete_employee/'.$id)?> " onclick="return confirm('are you sure want to delete')">
  <button type="button"  class="btn btn-danger">Delete</button></a></td>
                        </tr>
<?php } ?>



                      </tbody><tr>
                          <th>Candidate name</th>
                            <th>Profile picture</th>
                          
                        
                          <th>Action</th>
                        </tr>
                      <tfoot>
                        
                      </tfoot>
                    </table>
                  </div>
                </div>


    </div>
  </div>
</div>
</div>
