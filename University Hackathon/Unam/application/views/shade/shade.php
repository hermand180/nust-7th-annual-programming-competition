         

<div class="right_col" role="main">
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
 <a href="<?=base_url('welcome/add_shade')?>">
  <button type="button" class="btn btn-success">Add Shade+</button></a>
        <div class="clearfix"></div>
      </div>


                  <div class="x_content">
                    <div class="grid-body ">
            <div class="col-sm-12" style="margin-bottom:25px;">
                <?php 
                    if(!$this->session->flashdata('msg')==''){  ?>
                       <center>   
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>
                                <h4>  <?php  echo $this->session->flashdata('msg'); ?></h4>
                            </div>
                        </center>
                <?php } ?>

                <?php 
                if(!$this->session->flashdata('error_msg')==''){  ?>
                    <center>   
                        <div class="alert alert-danger alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>
                            <h4>  <?php  echo $this->session->flashdata('error_msg'); ?></h4>
                        </div>
                    </center>
                <?php } ?>
            </div> 
        </div>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Shade Name</th>
                          <th>Image</th>
                          <th>Color</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
<?php 
foreach ($result as $value) {
  $id=$value['id'];
  $image=$value['image'];

?>

                        <tr>
                          <td><?=$value['shade_name']; ?></td>
                          <td><img src="<?=site_url('images/shade_image/'.$image)?>" height="40px" width="40px"></td>
                          <td><div style="background-color: <?=$value['color_name']; ?>;height: 40px;width: 40px;"></div></td>

                           <td> <a href="<?=base_url('welcome/edit_shade/'.$id)?>">
  <button type="button" class="btn btn-primary">Edit</button></a>
 
<a href="<?=base_url('welcome/delete_shade/'.$id)?> " onclick="return confirm('are you sure want to delete')">
  <button type="button"  class="btn btn-danger">Delete</button></a></td>
                        </tr>
<?php } ?>



                      </tbody>
                          <th>Shade Name</th>
                          <th>Image</th>
                          <th>Color</th>
                          <th>Action</th>
                        </tr>
                      <tfoot>
                        
                      </tfoot>
                    </table>
                  </div>
                </div>


    </div>
  </div>
</div>
</div>
