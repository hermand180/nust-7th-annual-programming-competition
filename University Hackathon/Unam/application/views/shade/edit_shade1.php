<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/js/bootstrap-colorpicker.min.js"></script>  



<div class="right_col" role="main">

<div class="row">

  <div class="col-md-12 col-sm-12 col-xs-12">

    <div class="x_panel">

      <div class="x_title">

        <h2>Register Form </h2>

        <div class="clearfix"></div>

      </div>

      <div class="x_content">

        <div class="grid-body ">

            <div class="col-sm-12" style="margin-bottom:25px;">

                <?php 

                    if(!$this->session->flashdata('msg')==''){  ?>

                       <center>   

                            <div class="alert alert-success alert-dismissable">

                                <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>

                                <h4>  <?php  echo $this->session->flashdata('msg'); ?></h4>

                            </div>

                        </center>

                <?php } ?>



                <?php 

                if(!$this->session->flashdata('error_msg')==''){  ?>

                    <center>   

                        <div class="alert alert-danger alert-dismissable">

                            <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>

                            <h4>  <?php  echo $this->session->flashdata('error_msg'); ?></h4>

                        </div>

                    </center>

                <?php } ?>

            </div> 

        </div>

        <br />

        <form method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" action="<?=base_url('welcome/update_shade')?>">

          <?php foreach ($result as $key => $value) {

            $image=$value['image'];

          } ?>

          <input type="hidden" name="id" value="<?=$value['id']?>">

          <div class="form-group">

            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Shade Name <span class="required">*</span>

            </label>

            <div class="col-md-6 col-sm-6 col-xs-12">

              <input type="text" name="shade_name" id="shade_name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter Shade Name" value="<?=$value['shade_name']?>">

            </div>

          </div>

          

         


          <div class="form-group">

            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Color Pick<span class="required">*</span>

              </label>

              <div class="col-md-6 col-sm-6 col-xs-12">

                <div style="background-color: <?=$value['color_hex']; ?>;height: 40px;width: 40px;"></div>

                <input class="form-control" type="text" id="color" name="color_name" />

                <input type="hidden" name="old_color" value="<?=$value['color_hex']?>">

              </div>

          </div>



          <div class="ln_solid"></div>

          <div class="form-group">

            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

              <a href="<?=site_url('Welcome/shade')?>">

              <button class="btn btn-primary" type="button">Back</button></a>

              <input type="submit" name="Add Register" value="Update" class="btn btn-success">

            </div>

          </div>



        </form>

      </div>

    </div>

  </div>

</div>

</div>

<script>



    $('#color').colorpicker({});



</script>