<?php 
      $staff_query=$this->db->query("SELECT count('id') as total_voter from tbl_companies");
      $staffdata=$staff_query->result_array();
      $total_voter=$staffdata[0]['total_voter'];

      $staff_query1=$this->db->query("SELECT count('id') as total_p from  tbl_customer");
      $staffdata1=$staff_query1->result_array();
      $total_p=$staffdata1[0]['total_p'];

       $staff_query2=$this->db->query("SELECT count('id') as total_v from  tbl_voting_count where vote = 'Johannes Ndjimba'");
      $staffdata2=$staff_query2->result_array();
      $total_v=$staffdata2[0]['total_v'];

      $staff_query3=$this->db->query("SELECT count('id') as total_vc from  tbl_voting_count where vote = 'Linus Muuteyapo'");
      $staffdata3=$staff_query3->result_array();
      $total_cr=$staffdata3[0]['total_vc'];

       $staff_query4=$this->db->query("SELECT count('id') as total_va from  tbl_voting_count where vote = 'Sylvia Ndapandula'");
      $staffdata4=$staff_query4->result_array();
      $total_ca=$staffdata4[0]['total_va'];

      $staff_query5=$this->db->query("SELECT count('id') as total_nota from  tbl_voting_count where vote = 'Evarist Shaama'");
      $staffdata5=$staff_query5->result_array();
      $total_nota=$staffdata5[0]['total_nota'];

       $staff_query6=$this->db->query("SELECT count('id') as total_voting from  tbl_voting_count ");
      $staffdata6=$staff_query6->result_array();
      $total_voting=$staffdata6[0]['total_voting'];
 ?>  
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Voters <?php echo $total_voter ?></span>
              <div class="count"></div>
              <!-- <span class="count_bottom"><i class="green">4% </i> From last Week</span> -->
            </div>

               <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Voting <?php echo $total_voting ?></span>
              <div class="count"></div>
              <!-- <span class="count_bottom"><i class="green">4% </i> From last Week</span> -->
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Candidates <?php echo $total_p ?></span>
              <div class="count"></div>
              <!-- <span class="count_bottom"><i class="green">4% </i> From last Week</span> -->
            </div>




           
          </div>
       
          <br />

          <div class="row">


            <div class="col-md-6">
              <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                  <h2>Results:</h2>
                  <h2> Incoming SRC president!</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <h3></h3>                    


                   
                
                  <div class="widget_summary">
                   <table class="table">
                     
                     <tr><th>Candidate</th><th>Total Votes</th></tr>
                     <?php $n = 0;
                     foreach ($res as $value) { 
                            $n++;
                          $image=$value['last_name']; ?>

                     <tr><td><?=$value['first_name']; ?><img src="<?=site_url('logo/'.$image)?>" height="40px" width="40px"></td>
 
<?php if($n==1)  {?> <td><?php echo $total_cr ?></td> <?php } ?>
<?php if($n==2)  {?> <td><?php echo $total_v ?></td> <?php } ?> 
<?php if($n==3)  {?> <td><?php echo $total_ca ?></td> <?php } ?> 
<?php if($n==4)  {?> <td><?php echo $total_nota ?></td> <?php } ?>                      </tr>
                     <?php } ?>

                   </table>
                  </div>


                 
                
             
                </div>
              </div>
            </div>

            <div class="col-md-6 ">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <canvas id="myChart" ></canvas>
                
              </div>
            </div>

                 
         
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
        <script>
var xValues = ["Linus","John", "Sylvia", "Evarist"];
var yValues = [<?php echo $total_cr  ?> ,<?php echo $total_v ?>,  <?php echo $total_ca  ?>, <?php echo $total_nota  ?>];
var barColors = [
  "#b91d47",
  "#00aba9",
  "#2b5797",
  "#e8c3b9",
  "#1e7145"
];

new Chart("myChart", {
  type: "doughnut",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    title: {
      display: true,
      text: " NUST SRC Election Results 2022 "
    }
  }
});
</script>