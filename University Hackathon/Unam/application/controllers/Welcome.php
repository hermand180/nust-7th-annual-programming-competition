<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	    $this->load->library('upload');
		$this->load->library('session'); 

		

	}

	public function logout(){
		session_destroy();
        $this->load->view('common/login');	
	}

	public function login(){
         $this->load->view('common/login');
        // $this->load->view('vote/login');	
	}

	public function login_check(){
		$table='tbl_admin';
		$usermail=$this->input->post('email');
		$passwords=$this->input->post('password');
		$password=md5($passwords);
		$result=$this->Common_model->login_check($usermail,$password,$table);
		if($result)
		{
           redirect(base_url('welcome/index'));
		}
		else
		{
			redirect(base_url('welcome/login'));
		}
	}

	public function index()
    {
        
     $data['res'] = $this->Common_model->get_data('tbl_customer');
        $data['main']='common/content';
        $this->load->view('common/template',$data);
    } 
    


    public function profile(){
    	$id=$this->session->userdata('id');
    	$data['registration']=$this->Common_model->getdata_one('tbl_admin',$id);
    	$data['main']='common/profile';
    	$this->load->view('common/template',$data);
    } 


    public function Employees(){
		$data['result'] = $this->Common_model->get_data('tbl_customer');
		$data['main']='Employees/customer';
		$this->load->view('common/template',$data);

	}


	public function add_employee(){
		$data['main']='Employees/add_customer';
		$this->load->view('common/template',$data);
	}


	public function save_employee()
	{
		$table='tbl_customer';
		
		$image=time().$_FILES['last_name']['name'];
		$image_tmp=$_FILES['last_name']['tmp_name'];
		move_uploaded_file($image_tmp,'logo/'.$image);

		$save=array(
			'first_name'=> strtoupper($this->input->post('first_name')),
			
			'last_name'=>   $image
			// 'phone'=> $this->input->post('phone'),
			// 'email'=> $this->input->post('email'),

			
			// 'Companies' => $this->input->post('Companies')
			
		);
		$result=$this->Common_model->save_data($save,$table);

		if($result>0){
			$this->session->set_flashdata('msg','Party Added Successfully');
			redirect(base_url('Welcome/Employees'));
		}
		else{
   			 $this->session->set_flashdata('error_msg','Something Went Wrong!!! Try Again');
   			 redirect(base_url('Welcome/Employees'));
		}
	}


public function edit_employee(){
		$id=$this->uri->segment(3);
		$table='tbl_customer';
		$data['result'] = $this->Common_model->getdata_one($table,$id);
		$data['main']='Employees/edit_customer';
		$this->load->view('common/template',$data);
	}



	public function update_Employees(){
		$id=$this->uri->segment(3);
		$table='tbl_customer';
		$first_name=$this->input->post('first_name');
	
	
	 	$new_image=$_FILES['files']['name'];

	    $old_doc=$this->input->post('old_doc');

		if ($new_image!='') {
			$image=time().$_FILES['files']['name'];
			$tmp_name=$_FILES['files']['tmp_name'];
			move_uploaded_file($tmp_name,'logo/'.$image);

		}
		else{
			$image=$old_doc;
		}

	
		
		
	
		


		$save=array(
				'first_name'=>$first_name,
				
				'last_name'=>$image
				
				
			
		);

		

		$result=$this->Common_model->update_data($save,$table,$id);
		if($result>0){
			$this->session->set_flashdata('msg','Updated Successfully');
			 redirect(base_url('Welcome/Employees'));
		}
		else{
   			$this->session->set_flashdata('error_msg','Something Went Wrong!!! Try Again');
   			redirect(base_url('Welcome/Employees'));
		}
	}


	public function Companies(){
		$data['result'] = $this->Common_model->get_data('tbl_companies');
		$data['main']='customer/customer';
		$this->load->view('common/template',$data);

	}



	public function add_Companies(){
		$data['main']='customer/add_customer';
		$this->load->view('common/template',$data);
	}

	public function save_customer(){

		$table='tbl_companies';
		
		$image=time().$_FILES['files']['name'];
		$image_tmp=$_FILES['files']['tmp_name'];
		move_uploaded_file($image_tmp,'upload/'.$image);
		$originalDate =  $this->input->post('dob');

		$newDate = date("d-m-Y", strtotime($originalDate));

		$save=array(
			'name'=> $this->input->post('username'),
		
			'email'=>    $this->input->post('email'),
			'website'=>   $this->input->post('website'),
			'dob'=> $newDate ,
			'identity'=>   $this->input->post('identity'),
			
			
			'logo'=>   $image
		
		);

		$result=$this->Common_model->save_data($save,$table);
		if($result>0){
			$this->session->set_flashdata('msg','Added Successfully');
			 redirect(base_url('Welcome/Companies'));
		}
		else{
   			$this->session->set_flashdata('error_msg','Something Went Wrong!!! Try Again');
   			redirect(base_url('Welcome/Companies'));
		}
	}

	public function delete_customer(){
		$id=$this->uri->segment(3);
		$table='tbl_companies';
		$result=$this->Common_model->do_delete($table,$id);
		if($result>0){
		 $this->session->set_flashdata('msg','Deleted!');
	     redirect(base_url('welcome/Companies'));
	    }
	    else{
	      $this->session->set_flashdata('error_msg','Delete Failed!');
	      redirect(base_url('welcome/Companies'));
	    }
	}

	public function delete_employee(){
		$id=$this->uri->segment(3);
		$table='tbl_customer';
		$result=$this->Common_model->do_delete($table,$id);
		if($result>0){
		 $this->session->set_flashdata('msg','Deleted!');
	     redirect(base_url('welcome/Employees'));
	    }
	    else{
	      $this->session->set_flashdata('error_msg','Delete Failed!');
	      redirect(base_url('welcome/Employees'));
	    }
	}

	public function edit_Companies(){
		$id=$this->uri->segment(3);
		$table='tbl_companies';
		$data['result'] = $this->Common_model->getdata_one($table,$id);
		$data['main']='customer/edit_customer';
		$this->load->view('common/template',$data);
	}

	public function update_Companies(){
		$id=$this->uri->segment(3);
		$table='tbl_companies';
		$username=$this->input->post('username');
	
	
		$email=$this->input->post('email');
		
		$website=$this->input->post('website');

		 $new_dob=$this->input->post('new_dob');
		 $old_dob=$this->input->post('old_dob');
		

		if($new_dob=='')
	    {
         $date_of_birth = $old_dob;
	    }
	    else { 
	    	 $date_of_birth = $new_dob; 
	    	
	    }	
	
        $newDate = date("d-m-Y", strtotime($date_of_birth));

		$new_image=$_FILES['files']['name'];
	    $old_doc=$this->input->post('old_doc');

		if ($new_image!='') {
			$image=time().$_FILES['files']['name'];
			$tmp_name=$_FILES['files']['tmp_name'];
			move_uploaded_file($tmp_name,'upload/'.$image);

		}
		else{
			$image=$old_doc;
		}

	
		


		$save=array(
				'name'=>$username,
				
				'email'=>$email,
				
				'website'=>$website,
				
			    'dob' =>$newDate,
				
				'logo'=>$image,
			
		);

		

		$result=$this->Common_model->update_data($save,$table,$id);
		if($result>0){
			$this->session->set_flashdata('msg','Updated');
			 redirect(base_url('Welcome/Companies'));
		}
		else{
   			$this->session->set_flashdata('error_msg','Something Went Wrong!!! Try Again');
   			redirect(base_url('Welcome/Companies'));
		}
	}

	public function shade()
	{
		$data['result']=$this->Common_model->get_run("SELECT * from tbl_shade");
		$data['main']='shade/shade';
		$this->load->view('common/template',$data);
	}

	public function add_shade()
	{
		$data['main']='shade/add_shade';
		$this->load->view('common/template',$data);
	}
	
	public function edit_shade()
	{
		$id=$this->uri->segment(3);
		$data['result']=$this->Common_model->get_run("SELECT * from tbl_shade where id='$id'");
		$data['main']='shade/edit_shade';
		$this->load->view('common/template',$data);
	}
	public function update_shade()
	{
		$table='tbl_shade';
		$id=$this->input->post('id');
		$shade_name=$this->input->post('shade_name');
		$old_image=$this->input->post('old_image');
		$images=$_FILES['files']['name'];
		$image_tmp=$_FILES['files']['tmp_name'];
		$color_name=$this->input->post('color_name');
		$old_color=$this->input->post('old_color');
		
		if($images!='')
		 {
		    $image = time().$images;
			move_uploaded_file($image_tmp,"images/shade_image/".$image);
			unlink("images/shade_image/".$old_image);
				 	
         }
		else
		{
			$image=$old_image;	
		}
		if($color_name)
		{
			$color=$color_name;
		}
		else
		{
			$color=$old_color;
		}
		$update=array(
			'shade_name'=> $this->input->post('shade_name'),
			'image'=>   $image,
			'color_name' => $color
			
		);
		$result=$this->Common_model->update_data($update,$table,$id);

		if($result>0){
			$this->session->set_flashdata('msg','Updated');
			redirect(base_url('Welcome/shade'));
		}
		else{
   			 $this->session->set_flashdata('error_msg','Something Went Wrong!!! Try Again');
   			 redirect(base_url('Welcome/shade'));
		}
	}
	public function delete_shade()
	{
		$id=$this->uri->segment(3);
		$table='tbl_shade';
		$result_image=$this->Common_model->get_run("SELECT image from tbl_shade where id='$id'");
		$result=$this->Common_model->do_delete($table,$id);
		if($result>0){
			unlink("images/shade_image/".$result_image[0]['image']);
		 $this->session->set_flashdata('msg','Deleted');
	     redirect(base_url('welcome/shade'));
	    }
	    else{
	      $this->session->set_flashdata('error_msg','Delete Failed!');
	      redirect(base_url('welcome/shade'));
	    }
	}
	public function building(){
		$data['result'] = $this->Common_model->get_data('tbl_building');
		$data['main']='building/building';
		$this->load->view('common/template',$data);

	}

	public function add_building(){
		$data['main']='building/add_building';
		$this->load->view('common/template',$data);
	}


	public function save_building(){

		$table='tbl_building';
		
		$image=time().$_FILES['files']['name'];
		$image_tmp=$_FILES['files']['tmp_name'];
		move_uploaded_file($image_tmp,'upload/'.$image);

		$save=array(
			'building_name'=> $this->input->post('building_name'),
			
			
			'contact'=>   $this->input->post('contact'),
			
			'address'=>	 $this->input->post('address'),

			'image'=>   $image
			
		);

		$result=$this->Common_model->save_data($save,$table);

		if($result>0){
			$this->session->set_flashdata('msg','Added');
			redirect(base_url('Welcome/building'));
		}
		else{
   			 $this->session->set_flashdata('error_msg','Something Went Wrong!!! Try Again');
   			 redirect(base_url('Welcome/building'));
		}
		
	}
	public function delete_building(){
		$id=$this->uri->segment(3);
		$table='tbl_building';
		$result=$this->Common_model->do_delete($table,$id);
		if($result>0){
		 $this->session->set_flashdata('msg','Deleted!');
	     redirect(base_url('welcome/building'));
	    }
	    else{
	      $this->session->set_flashdata('error_msg','Delete Failed!');
	      redirect(base_url('welcome/building'));
	    }
	}
	public function edit_building(){
		$id=$this->uri->segment(3);
		$table='tbl_building';
		$data['result'] = $this->Common_model->getdata_one($table,$id);
		$data['main']='building/edit_building';
		$this->load->view('common/template',$data);
	}

	public function put_building(){
		$id=$this->uri->segment(3);
		$table='tbl_building';
		$buildingname=$this->input->post('building_name');
		
		$address=$this->input->post('address');
		$contact=$this->input->post('contact');
		
		$new_image=$_FILES['files']['name'];
	    $old_doc=$this->input->post('old_doc');

		if ($new_image!='') {
			$image=time().$_FILES['files']['name'];
			$tmp_name=$_FILES['files']['tmp_name'];
			move_uploaded_file($tmp_name,'upload/'.$image);

		}
		else{
			$image=$old_doc;
		}

		


		$save=array(
				'building_name'=>$buildingname,
				
				'address'=>$address,
				'contact'=>$contact,
				
				'image'=>$image
				
		);

		$result=$this->Common_model->update_data($save,$table,$id);
		if($result>0){
			$this->session->set_flashdata('msg','Updated!');
			redirect(base_url('Welcome/building'));
		}
		else{
   			 $this->session->set_flashdata('error_msg','Something Went Wrong!!! Try Again');
   			 redirect(base_url('Welcome/building'));
		}
		
	}
	 public function Forget_Password(){
    	 $this->load->view('common/forget_password');	
    }


	public function password_change(){
		$table='tbl_admin';
		$email=$this->input->post('email');
		$result=$this->Common_model->Change_Forget_Password($email);

		if(!($result== -1)){
			$id=$result[0]['id'];
			$random_pass=rand();
			$new_pass=md5($random_pass);
			$random=array('password'=>$new_pass );
			$result=$this->Common_model->update_for_all($random,$table,$id,'id');

			$this->email->from('admin@admin.com', 'testing'); 
			 $this->email->to($email);
			 $this->email->subject('Forget Password'); 
			 $this->email->message('Your New password is:'.$random_pass); 
			 if($this->email->send()) {
					$this->Common_model->my_success('Email Sent Successfully');
			}
			 else{ 
				$this->Common_model->my_failed();
			}
		}
		else{
			$this->Common_model->my_failed();
		}
		$this->Common_model->my_return();
	}


public function vote(){
        // $this->load->view('common/login');
        $this->load->view('vote/login');	
	}

    
public function voting_login_check(){
		$table='tbl_companies';

		$id=$this->input->post('id');
		
		
		$result=$this->Common_model->voting_login_check($id,$table);
		
		
		if($result != -1)
		{ 
			
			
           redirect(base_url('welcome/voter_view'));
		}
		else
		{   $this->session->set_flashdata('error_msg','Something Went Wrong!!! Try Again');
			redirect(base_url('welcome/vote'));

		}

	}




		public function voter_view(){
        // $this->load->view('common/profile');
			 $identity = $this->session->userdata('identity');
 if($identity){
 $data['result']=$this->Common_model->get_run("SELECT * from tbl_customer");
 $data['res']=$this->Common_model->get_run('SELECT * from tbl_companies where identity = '."$identity".'');	
 // $this->db->last_query();
}
        $this->load->view('vote/profile',$data);	
	}


	public function save_vote()
	{
		$table='tbl_voting_count';
		
		$vote =  $this->input->post('vote');
		
		$v = implode('  ', $vote);

		$save=array(
			'identity'=> $this->input->post('identity'),
			'vote'=>$v
		);

	
		$result=$this->Common_model->save_data($save,$table);

		if($result>0){		
			$this->session->set_flashdata('msg','You Voted Successfully');
			redirect(base_url('Welcome/vote'));

		}
		else{
   			 $this->session->set_flashdata('error_msg','Something Went Wrong!!! Try Again');
   			 
   			 redirect(base_url('Welcome/vote'));
		}
	}

public function voter_check()
	{
		$identity=$this->input->post('s_id');
		
		$table='tbl_voting_count';
	    $data = $this->Common_model->get_run("SELECT * from tbl_voting_count where identity ='$identity' ");
	    
	    if ($data) { echo 1;	} else echo 2;
	}
	      
        
public function Print_voter(){
        // $this->load->view('common/login');
	$data['result']=$this->Common_model->get_run("SELECT * from tbl_companies");
        $this->load->view('vote/Print_voter',$data);	
	}



}
