<?php

class Common_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('session'); 
	}

	public function login_check($email,$password)
	{
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$query = $this->db->get('tbl_admin');
		

		if($query->num_rows() == 1)  
		{

			$result=$query->result();
			$sessiondata = array(
									'id'  =>$result[0]->id,
									'username'  =>$result[0]->username,
									'email'  =>$result[0]->email,
									'role'  =>$result[0]->role,
									'image'  =>$result[0]->image             
			                    );
			$this->session->set_userdata($sessiondata);
			return $result[0]->id;
		} 
		else
		{		
     		return -1;
		}
	}


	public function voting_login_check($id,$table)
	{
		
		$this->db->where('identity',$id);
		$query = $this->db->get('tbl_companies');
		
		

		if($query->num_rows() == 1)  
		{

			$result=$query->result();
			$sessiondata = array(
									'id'  =>$result[0]->id,
									 'logo'  =>$result[0]->logo,
									 'email'  =>$result[0]->email,
									'identity'  =>$result[0]->identity
									// 'image'  =>$result[0]->image             
			                    );
			$this->session->set_userdata($sessiondata);
			return $result[0]->id;
		} 
		else
		{		
     		return -1;
		}
	}





	public function session_check()
	{
		if($this->session->userdata('id')=='')
		{
		    return false;
		}
		else
		{
     		return true;        

		}
	}



	public function save_data($insert_data,$table)
	{
		$this->db->insert($table,$insert_data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

		public function get_data($table){
		$query=$this->db->get($table);
		return $query->result_array();
	}

		public function do_delete($table,$id){
		$this->db->where('id',$id);
		$this->db->delete($table);
		return ($this->db->affected_rows()!=1)? false : true;
	}

	public function getdata_one($table,$id)
	{
		$this->db->where('id',$id);
		$q = $this->db->get($table);
		return $result = $q->result_array();
	}

	public function update_data($save,$table,$id) 
	{
	    $this->db->where('id', $id);
	    $data=$this->db->update($table, $save);
		if($data)
		   return true;
		else
		    return false;
	}

	public function my_success($msg)
    {
     	$this->session->set_flashdata('msg',$msg);
    }

    public function my_failed()
    {
     	$this->session->set_flashdata('error_msg','Something Went Wrong!!! Try Again');
    } 

    public function my_return()
    {
     	redirect($_SERVER['HTTP_REFERER']);
    } 

    public function get_run($run)
	{
		$query = $this->db->query($run);
		return $query->result_array();	
	}


    function upload_file($path,$image_name,$tmp_name)
    {
		if(move_uploaded_file($tmp_name,$path.$image_name) && is_writable($path))
		{
		    $display_message = 1;
		    $display_message = $path.$image_name;
		}
		else
		{
	    	$display_message = '';
		}
       return $display_message;
    }

    public function Change_Forget_Password($email){
		$this->db->where('email',$email);
		$query = $this->db->get('tbl_admin');
		if($query->num_rows() == 1)  {
		$result=$query->result_array();
		return $result;
		} 
		else{   
		return -1;
		}
	}

	public function update_for_all($update_data,$table,$id,$col_name)
	{
		$this->db->where($col_name,$id);
		return ( $this->db->update($table, $update_data) != 1) ? false : true;
	}
	

	public function getdata_one_vote($table,$identity)
	{
		$this->db->where('identity',$identity);
		$q = $this->db->get($table);
		return $result = $q->result_array();
	}






}


?>