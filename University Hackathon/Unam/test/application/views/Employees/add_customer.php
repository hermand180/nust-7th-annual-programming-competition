
<div class="right_col" role="main">
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Register Form </h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="grid-body ">
            <div class="col-sm-12" style="margin-bottom:25px;">
                <?php 
                    if(!$this->session->flashdata('msg')==''){  ?>
                       <center>   
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>
                                <h4>  <?php  echo $this->session->flashdata('msg'); ?></h4>
                            </div>
                        </center>
                <?php } ?>

                <?php 
                if(!$this->session->flashdata('error_msg')==''){  ?>
                    <center>   
                        <div class="alert alert-danger alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>
                            <h4>  <?php  echo $this->session->flashdata('error_msg'); ?></h4>
                        </div>
                    </center>
                <?php } ?>
            </div> 
        </div>
        <br />
        <form method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" action="<?=base_url('welcome/save_employee')?>">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Party Name <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="first_name" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter username">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">logo <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="file" name="last_name" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter username">
            </div>
          </div>

<!-- 
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">phone <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="phone" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter username">
            </div>
          </div>
        
          <div class="form-group">
            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="middle-name" class="form-control col-md-7 col-xs-12" type="email" name="email" placeholder="Enter Email">
            </div>
          </div>

         
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Companies<span class="required" >*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12"><select class="form-control" name="Companies"><option >select</option>
              <?php $data = $this->Common_model->get_run("SELECT * from   tbl_companies "); 

                                     foreach ($data as $key => $value) {
                                         
                                     
                                     ?>    
                                    <option value="<?php echo $value['id'] ?>"><?php echo $value['name'] ?> </option>
                                    
                                    
                                    <?php } ?>
                                    </select>
            </div>
          </div> -->
          <!-- <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div id="gender" class="btn-group" data-toggle="buttons">
                <label class="btn btn-success" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input type="radio" name="gender" value="male"> &nbsp; Male &nbsp;
                </label>
                <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input type="radio" name="gender" value="female"> Female
                </label>
              </div>
            </div>
          </div> -->
         
         
        

         
        


          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <a href="<?=site_url('Welcome/Employees')?>">
              <button class="btn btn-primary" type="button">Back</button></a>
              <input type="submit" name="Add Register" value="Add " class="btn btn-success">
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
</div>