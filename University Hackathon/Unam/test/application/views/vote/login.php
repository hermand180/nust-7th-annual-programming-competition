<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<style>
.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   
   color: white;
   text-align: center;
}
</style>
	<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Raleway:400,700');

* {
	box-sizing: border-box;
	margin: 0;
	padding: 0;	
	font-family: Raleway, sans-serif;
}

body {
	background: linear-gradient(90deg, #C7C5F4, #776BCC);		
}

.container {
	display: flex;
	align-items: center;
	justify-content: center;
	min-height: 100vh;
}

.screen {		
	background: linear-gradient(90deg, #5D54A4, #7C78B8);		
	position: relative;	
	height: 600px;
	width: 360px;	
	box-shadow: 0px 0px 24px #5C5696;
}

.screen__content {
	z-index: 1;
	position: relative;	
	height: 100%;
}

.screen__background {		
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 0;
	-webkit-clip-path: inset(0 0 0 0);
	clip-path: inset(0 0 0 0);	
}

.screen__background__shape {
	transform: rotate(45deg);
	position: absolute;
}

.screen__background__shape1 {
	height: 520px;
	width: 520px;
	background: #FFF;	
	top: -50px;
	right: 120px;	
	border-radius: 0 72px 0 0;
}

.screen__background__shape2 {
	height: 220px;
	width: 220px;
	background: #6C63AC;	
	top: -172px;
	right: 0;	
	border-radius: 32px;
}

.screen__background__shape3 {
	height: 540px;
	width: 190px;
	background: linear-gradient(270deg, #5D54A4, #6A679E);
	top: -24px;
	right: 0;	
	border-radius: 32px;
}

.screen__background__shape4 {
	height: 400px;
	width: 200px;
	background: #7E7BB9;	
	top: 420px;
	right: 50px;	
	border-radius: 60px;
}

.login {
	width: 320px;
	padding: 30px;
	padding-top: 156px;
}

.login__field {
	padding: 20px 0px;	
	position: relative;	
}

.login__icon {
	position: absolute;
	top: 30px;
	color: #7875B5;
}

.login__input {
	border: none;
	border-bottom: 2px solid #D1D1D4;
	background: none;
	padding: 10px;
	padding-left: 24px;
	font-weight: 700;
	width: 75%;
	transition: .2s;
}

.login__input:active,
.login__input:focus,
.login__input:hover {
	outline: none;
	border-bottom-color: #6A679E;
}

.login__submit {
	background: #fff;
	font-size: 14px;
	margin-top: 30px;
	padding: 16px 20px;
	border-radius: 26px;
	border: 1px solid #D4D3E8;
	text-transform: uppercase;
	font-weight: 700;
	display: flex;
	align-items: center;
	width: 100%;
	color: #4C489D;
	box-shadow: 0px 2px 2px #5C5696;
	cursor: pointer;
	transition: .2s;
}

.login__submit:active,
.login__submit:focus,
.login__submit:hover {
	border-color: #6A679E;
	outline: none;
}

.button__icon {
	font-size: 24px;
	margin-left: auto;
	color: #7875B5;
}

.social-login {	
	position: absolute;
	height: 140px;
	width: 160px;
	text-align: center;
	bottom: 0px;
	right: 0px;
	color: #fff;
}

.social-icons {
	display: flex;
	align-items: center;
	justify-content: center;
}

.social-login__icon {
	padding: 20px 10px;
	color: #fff;
	text-decoration: none;	
	text-shadow: 0px 0px 8px #7875B5;
}

.social-login__icon:hover {
	transform: scale(1.5);	
}
	</style>
	<style>
.all-browsers {
  margin: 0;
  padding: 5px;
  background-color: lightgray;
}

.all-browsers > h1, .browser {
  margin: 10px;
  padding: 5px;
}

.browser {
  background: white;
}

.browser > h2, p {
  margin: 4px;
  font-size: 90%;
}

footer {
  text-align: center;
  padding: 3px;
  background-color: DarkSalmon;
  color: white;
}
</style>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	<nav class="navbar navbar-inverse" style="background-color: #756bc3; color: white;">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#" style="color:white;"><strong>Online Voting System</strong></a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="https://finalyearstudentsprojectsinphp.blogspot.com/"><strong>Home</strong></a></li>
      <li><a href="<?php echo site_url('Welcome/login') ?>"><strong style="color:white;">Admin Penal</strong></a></li>
      <li><a href="https://www.youtube.com/channel/UCY3uXT1d09oytMM_8VDVzbg"><strong>More Projects</strong></a></li>
      <li><a href="https://finalyearstudentsprojectsinphp.blogspot.com/"><strong>About Us</strong></a></li>
      <li><a href="https://finalyearstudentsprojectsinphp.blogspot.com/"><strong>developed by</strong></a></li>
    </ul>
  </div>
</nav>
 <div class="col-sm-12" style="margin-bottom:25px;">
                <?php 
                    if(!$this->session->flashdata('msg')==''){  ?>

                       <center>   
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>
                                <h4>  <?php  echo $this->session->flashdata('msg'); ?></h4>
                           <script type="text/javascript">alert('<?php  echo $this->session->flashdata('msg'); ?>')</script>
                           <?php session_destroy(); ?>
                            </div>
                        </center>

                <?php } ?>

                <?php 
                if(!$this->session->flashdata('error_msg')==''){  ?>
                    <center>   
                        <div class="alert alert-danger alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>
                            <h4>  <?php  echo $this->session->flashdata('error_msg'); ?></h4>
                            <script type="text/javascript">alert('<?php  echo $this->session->flashdata('error_msg'); ?>')</script>
                        </div>
                    </center>
                <?php } ?>
            </div> 
<div class="container">
<div class="row">
<div class="col-md-6">
	<img src="<?=site_url('logo/'.'db.png')?>"  width="100%">
</div>
<div class="col-md-6">
	<div class="screen">
		<div class="screen__content">
			<form class="login" action="<?php echo base_url('Welcome/voting_login_check') ?>" method='post'>
				<div class="login__field">
					<i class="login__icon fas fa-user"> </i> <h1 style="color: blueviolet;"><b >Welcome to Online Voting System</b></h1>
					<!-- <input type="text" class="login__input" placeholder="User name / Email"> -->
				</div>
				<div class="login__field">
					<i class="login__icon fas fa-lock"><strong style="color: red;">Enter Voter ID Card No</strong></i> 
					<input type="password" onkeyup="return myfun()" id='s_id' class="button login__submit" name="id" placeholder="Enter Voter ID Card No" required>
				</div>
				<div class="login__field" >
					
					
				</div>
				<button class="button login__submit" style="background-color: #756bc3; color: white;">
					<span class="button__text">Submit Now</span>
					<i class="button__icon fas fa-chevron-right"></i>
				</button>				
			</form>
			<div class="social-login"><br>
				<h3 id="showreport1"></h3>
				<div class="social-icons">
					<a href="#" class="social-login__icon fab fa-instagram"></a>
					<a href="#" class="social-login__icon fab fa-facebook"></a>
					<a href="#" class="social-login__icon fab fa-twitter"></a>
				</div>
			</div>
		</div>
		<div class="screen__background">
			<span class="screen__background__shape screen__background__shape4"></span>
			<span class="screen__background__shape screen__background__shape3"></span>		
			<span class="screen__background__shape screen__background__shape2"></span>
			<span class="screen__background__shape screen__background__shape1"></span>
		</div>		
	</div>
</div>	
</div>
	
</div>



</body>

<div class="footer">
 <strong> <p>developed by<a href="https://finalyearstudentsprojectsinphp.blogspot.com/"> dipesh baradiya</a></p></strong>
</div>

</html>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">

function myfun(){
var s_id=$('#s_id').val();

 // alert(s_id);
$.ajax({
type:'POST',
data:{'s_id':s_id},
url:'<?php echo base_url('Welcome/voter_check') ?>',
 success:function(result){
// $('#showreport1').html(result);
if(result==1)
{ alert('this identry no already exists')
	
	  
	$('#s_id').val('');
	return false;
}
 }
 });
}

</script>
<script type="text/javascript">
  jQuery(document).ready(function () {
      jQuery("#s_id").keypress(function (e) {
         var length = jQuery(this).val().length;
       if(length > 7) {
            return false;
       } else if(e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
       } else if((length == 0) && (e.which == 48)) {
            return false;
       }
      });
    });
</script>