
<div class="right_col" role="main">
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Profile </h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
<?php
foreach ($registration as $value) {
$id=$value['id'];
}
?>

       <form method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" action="<?=base_url('welcome/put_register/'.$id)?>">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">User Name <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="username" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter username" value="<?=$value['username']?>">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="password" id="last-name" name="password"  class="form-control col-md-7 col-xs-12" placeholder="Enter New Password">
         <input type="hidden" name="old_password" value="<?=$value['password']?>">
            </div>
          </div>
          <div class="form-group">
            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="middle-name" class="form-control col-md-7 col-xs-12" type="email" name="email" placeholder="Enter Email" value="<?=$value['email']?>">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mobile<span class="required" >*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="mobile" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter Mobile Number" value="<?=$value['mobile']?>">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <!-- <div id="gender" class="btn-group" data-toggle="buttons"> -->
               <!--  <label class="btn btn-success" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default"> -->
                <label class="control-label">
                  <input type="radio" name="gender" value="male" <?php
                  $gender=$value['gender'];
                  if($gender=='male'){
                    echo "checked";
                  }
                  ?>>&nbsp;Male &nbsp;
                </label>
               <!--  <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default"> -->
                <label class="control-label">
                  <input type="radio" name="gender" value="female" <?php
                  $gender=$value['gender'];
                  if($gender=='female'){
                    echo "checked";
                  }
                  ?>>&nbsp;Female
                </label>
              <!-- </div> -->
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="birthday" name="dob" class="date-picker form-control col-md-7 col-xs-12" required="required" type="date" value="<?=$value['dob']?>">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Address<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">

              <textarea name="address" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter Address"><?=$value['address']?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <img src="<?=base_url('upload/'.$value['image'])?>" height="60px" width="60px">
              <input type="file" name="files">
              <input type="hidden" name="old_doc" value="<?=$value['image']?>">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Role<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select class="form-control" name="role">
                <option value="1" <?php echo ($value['role']=='1')?'selected':'' ?> >Admin</option>
                <option value="0" <?php echo ($value['role']=='0')?'selected':'' ?>>User</option>
              </select>
            </div>
          </div>


          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <a href="<?=site_url('Welcome/register')?>">
              <button class="btn btn-primary" type="button">Back</button></a>
              <input type="submit" name="Add Register" class="btn btn-success"  value="Update Profile">
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
</div>