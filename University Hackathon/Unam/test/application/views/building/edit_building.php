
<div class="right_col" role="main">
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Edit Building </h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="grid-body ">
            <div class="col-sm-12" style="margin-bottom:25px;">
                <?php 
                    if(!$this->session->flashdata('msg')==''){  ?>
                       <center>   
                            <div class="alert alert-success alert-dismissable">
                                <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>
                                <h4>  <?php  echo $this->session->flashdata('msg'); ?></h4>
                            </div>
                        </center>
                <?php } ?>

                <?php 
                if(!$this->session->flashdata('error_msg')==''){  ?>
                    <center>   
                        <div class="alert alert-danger alert-dismissable">
                            <a href="" class="close" data-dismiss="alert" aria-label="close">x</a>
                            <h4>  <?php  echo $this->session->flashdata('error_msg'); ?></h4>
                        </div>
                    </center>
                <?php } ?>
            </div> 
        </div>
        <br />
<?php
foreach ($result as  $value) {
 $id=$value['id'];
}
?>

        <form method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" action="<?=base_url('welcome/put_building/'.$id)?>">

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Building Name <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="building_name" id="first-name" required="required" class="form-control col-md-7 col-xs-12" value="<?=$value['building_name']?>">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Address<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">

              <textarea name="address" id="first-name" required="required" class="form-control col-md-7 col-xs-12" ><?=$value['address']?></textarea>
            </div>
          </div>
          
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contact<span class="required" >*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="contact" id="first-name" required="required" class="form-control col-md-7 col-xs-12"  value="<?=$value['contact']?>">
            </div>
          </div>
          
          

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <img src="<?=base_url('upload/'.$value['image'])?>" height="60px" width="60px">
              <input type="file" name="files">
              <input type="hidden" name="old_doc" value="<?=$value['image']?>">
            </div>
          </div>
          


          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <a href="<?=site_url('Welcome/building')?>">
              <button class="btn btn-primary" type="button">Back</button></a>
              <input type="submit" name="Add Register" class="btn btn-success"  value="Update Building">
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
</div>