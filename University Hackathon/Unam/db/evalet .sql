-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 25, 2022 at 09:13 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `evalet`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL COMMENT '1 Admin,2 User'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `email`, `mobile`, `gender`, `dob`, `address`, `image`, `role`) VALUES
(2, 'ccvcvc', '827ccb0eea8a706c4c34a16891f84e7b', 'admin@admin.com', '', '', '', '', '1609771916Koala.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_companies`
--

CREATE TABLE `tbl_companies` (
  `id` int(100) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL COMMENT 'gender',
  `logo` varchar(11111) NOT NULL,
  `website` varchar(1111) NOT NULL COMMENT 'address',
  `identity` varchar(30) NOT NULL,
  `dob` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_companies`
--

INSERT INTO `tbl_companies` (`id`, `name`, `email`, `logo`, `website`, `identity`, `dob`) VALUES
(6, 'dipesh baradiya', 'female', '1660977618download (1).jpg', 'indore', '999978187703', '27-08-2022'),
(8, 'sushant singh', 'male', '1661347711download (1).jpg', 'indore', '999932601119', '19-08-2022'),
(9, 'radhu baradiya', 'female', '1661347700download.jpg', 'indore', '999959547387', '28-08-2022'),
(10, 'radha', 'female', '1661347693download (2).jpg', 'indore', '999946511843', '23-08-2022'),
(11, 'dipesh baradiya', 'male', '1661347686download (1).jpg', 'indore', '999912322704', '13-08-2022'),
(12, 'aayush', 'male', '1661347837download.jpg', 'indore', '999923976660', '27-08-2022'),
(13, 'aditi', 'female', '1661348941download (2).jpg', '30 indre', '999995814514', '03-01-2022'),
(14, 'mohan', 'male', '1661446890download.jpg', '30 indre', '999971764018', '10-08-2022'),
(15, 'dipesh', 'male', '1661447091download (2).jpg', '10 indore', '999998069750', '17-08-2022'),
(16, 'ram', 'male', '1661453060download.jpg', '30 indre', '999944915591', '18-08-2022');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `id` int(11) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL COMMENT 'logo',
  `phone` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `Companies` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`id`, `first_name`, `last_name`, `phone`, `email`, `Companies`) VALUES
(1, 'CONGRESS', '1661071893download.png', '4444', 'admin@admin.com', '3'),
(2, 'BJP', '1661071789BJP.png', '', '', ''),
(3, 'AAP', '1661071909images.jpg', '', '', ''),
(4, 'NOTA BUTTON', '1661071934nota.png', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_voting_count`
--

CREATE TABLE `tbl_voting_count` (
  `id` int(100) NOT NULL,
  `vote` varchar(100) NOT NULL,
  `identity` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_voting_count`
--

INSERT INTO `tbl_voting_count` (`id`, `vote`, `identity`) VALUES
(22, 'BJP', '999959547387'),
(23, 'CONGRESS', '999932601119'),
(24, 'AAP', '999978187703'),
(26, 'BJP', '999946511843'),
(27, 'NOTA BUTTON', '999912322704'),
(28, 'NOTA BUTTON', '999923976660'),
(29, 'BJP', '999995814514'),
(30, 'NOTA BUTTON', '999971764018'),
(31, 'BJP', '999998069750'),
(32, 'CONGRESS', '999944915591');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_companies`
--
ALTER TABLE `tbl_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_voting_count`
--
ALTER TABLE `tbl_voting_count`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_companies`
--
ALTER TABLE `tbl_companies`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_voting_count`
--
ALTER TABLE `tbl_voting_count`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
